<?php
include 'error.php';
class Game
{
	public $player;
	public $plateau = 'I ';
	private $nbrstick ;
	public $set;

	public function __construct()
	{
		$this->player = "Player 1";
		$this->rule();
		$this->set = $this->game_mod();
		$line = $this->game_stick();
	}

	public function game_mod() // CHOICE PLAYER / IA 
	{
		$set = readline("Add a player[1]  Play against IA[2] ?");
		switch ($set) {
			case 1:
			return 1;
			break;
			case 2:
			return 2;
			break;
			default:
			return $this->game_mod();
			break;
		}
	}

	public function game_stick() // CHOICE STICKS 
	{
		$line = readline("How many sticks do you want ?");

		if(($line >= 10) AND ($line <= 1000))
		{
			$this->nbrstick = $line;
			$this->sticks();
			echo $this->player . " is your turn! Be ready.";
			if($this->set == 1)
				$this->gameplayer();
			else
				$this->gameia();
		}
		else
		{
			echo "The game must have between 10 and 1000 sticks". PHP_EOL;
			$this->game_stick();
		}
	}


	public function sticks() // DISPLAY STICKS
	{
		for($i = 1; $i <= $this->nbrstick; $i++)
			echo $this->plateau;
		
		echo PHP_EOL;
	}

	public function turn() // TURN BETWEEN PLAYERS
	{
		$this->player = $this->player == "Player 1" ? "Player 2" : "Player 1";
		echo PHP_EOL;
		echo $this->player . " à toi de jouer";
	}
	public function turnia() // TURN BETWEEN PLAYER/IA
	{
		$this->player = $this->player == "Player 1" ? "AI" : "Player 1";
		echo PHP_EOL;
		echo $this->player . " à toi de jouer";
	}

	public function ia() // IA FUNCTION
	{
		$size = $this->nbrstick;
		$max_palier = null;
		for($i=0; $i < $size; $i++)
		{
			$pallier = 1+((3+1)*$i);
			if($pallier < $size)
			{
				$max_palier = $pallier;
			}
			elseif($size == $pallier)
			{
				return 1;
			}
			else
				return $size - $max_palier;
		}
	}

	public function gameplayer() // GAME ONLY PLAYERS
	{
		echo PHP_EOL;
		$stick = (int)readline("Matches : ");
		$error = new Errors();
		if($error->check_number($stick, $this->nbrstick))
		{
			$result = $this->nbrstick = $this->nbrstick - $stick;

			echo PHP_EOL;
			echo $this->player . " removed ". $stick . " matches" . PHP_EOL;
			echo "rest : ". $result . PHP_EOL;
			$this->sticks();
			echo PHP_EOL;
			if($this->nbrstick <= 1)
			{
				$this->win();
			}
			$this->turn();
			$this->gameplayer();
			echo PHP_EOL;
		}
		else
		{	
			$this->gameplayer();
		}
	}

	public function gameia() // GAME PLAYER / IA 
	{
		echo PHP_EOL;
		if($this->player == "Player 1")
		{
			$stick = (int)readline("Matches : ");
		}
		else
		{
			$stick = $this->ia();
		}
		$error = new Errors();
		if($error->check_number($stick, $this->nbrstick))
		{
			$result = $this->nbrstick = $this->nbrstick - $stick;
			echo PHP_EOL;
			echo $this->player . " removed ". $stick . " matches" . PHP_EOL;
			echo "rest : ". $result . PHP_EOL;
			$this->sticks();
			echo PHP_EOL;
			if($this->nbrstick <= 1)
			{
				$this->win();
			}
			$this->turnia();
			$this->gameia();
			echo PHP_EOL;
		}
		else
		{	
			$this->gameia();
		}
	}

	public function win() // CHECK WIN
	{
		echo PHP_EOL;
		if( $this->player == "Player 1")
		{
			echo "I lost ... snif ... but I'll get you next time !!". PHP_EOL;
		}
		else
			echo "You lost , too bad ..." . PHP_EOL;
		$restart = readline("Restart the game ?  yes[1] no[2]");
		switch ($restart) {
			case '1':
			$this->__construct();
			break;
			case '2':
			exit;
			break;
			default;
			$this->win();
			break;
		}
	}

	public function rule() // RULES OF THE GAME
	{
		echo PHP_EOL;
		echo PHP_EOL;
		echo "WELCOME IN THE STICKS'S GAME" . PHP_EOL. PHP_EOL;
		echo "Rules of the Game" . PHP_EOL. PHP_EOL;
		echo "There is a small pile of matches on the table. Each player takes turns either 1, 2 or 3 matches.".PHP_EOL."The player who takes the last match has lost. To win, you have to leave the last match to your opponent.";
		echo PHP_EOL;
		echo PHP_EOL;
	}
}
$foo = new Game();
?>