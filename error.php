<?php 

class Errors
{
	public function check_number($num, $nbrstick)
	{
		if($num > $nbrstick)
		{
			echo "Error : not enough matches";
			return false;
		}
		elseif($num == 0)
		{
			echo "you have to remove at least one match";
			return false;
		}
		elseif(($num < 1) || ($num >= 4) || !is_int($num))
		{
			echo "Error : invalid input ( positive number expected )";
			return false;
		}
		return true;
	}
}

?>